﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace sandti_mvc.Models
{
    public class Job
    {
        public int Id { get; set; }
        [Display(Name = "اسم الوظيفة", Description = "اسم الوظيفة")]
        public string JobTitle { get; set; }
        [Display(Name = "وصف الوظيفة", Description = "وصف الوظيفة")]
        [AllowHtml]
        public string JobContent { get; set; }
        [Display(Name = "صورة الوظيفة", Description = "صورة الوظيفة")]
        public string JobImage { get; set; }
        [Display(Name = "فئة الوظيفة", Description = "فئة الوظيفة")]
        public int CategoryID { get; set; }
        public string UserID { get; set; }
        public virtual Category Category { get; set; }
        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<ApplyForJob> ApplyJobs { get; set; }


    }
}